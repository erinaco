CFLAGS ?= -O2 -Wall -Wextra
erinaco-data: memmap.o ustr-extra.a file.o open.o -lustr -lcdb

ustr-extra.a: ustr-extra/ustr_basename.o ustr-extra/ustr_filename.o
	ar rs $@ $^
