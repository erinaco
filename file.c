#include <stdbool.h>
#include <unistd.h>
#include "file.h"

bool exists(const char *pathname)
{
	return access(pathname, X_OK) == 0;
}
