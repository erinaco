#include <dirent.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <cdb.h>
#include <ustr.h>

#include "open.h"
#include "file.h"
#include "dirent.h"
#include "memmap.h"
#include "ustr-extra.h"

#define write2(x) write(2, x"", sizeof(x))

static struct cdb_make cdb;
static const char *hostname;

extern void process_directory(const Ustr *);


int scandir_filter (const struct dirent *entry)
{
	return entry->d_name[0] != '.';
}

void gophermap_add_entry(Ustr **gophermap, const Ustr *path, char c)
{
	Ustr *name = ustr_filename(path);

	ustr_add_fmt(gophermap, "%c%s\t%s\t%s\t70\r\n",
	             c, ustr_cstr(name), ustr_cstr(path) + 1, hostname);
	ustr_free(name);
}



void process_dirent_regular(Ustr *path, Ustr **gophermap)
{
	struct memory mem;
	int err;
	int fd;

	fd = open_read(ustr_cstr(path));
	if (fd < 0) {
		write2("fatal: open() failed\n");
		exit(1);
	}
	err = mem_map_read(&mem, fd);
	if (err != 0) {
		write2("fatal: failed to mmap file\n");
		exit(1);
	}
	cdb_make_add(&cdb, ustr_cstr(path) + 1, ustr_len(path) - 1, mem.data, mem.length);
	mem_unmap(&mem);

	gophermap_add_entry(gophermap, path, '0');
}

void process_dirent_directory(const Ustr *path, Ustr **gophermap)
{
	Ustr *name;
	Ustr  *dirname;

	name    = ustr_filename(path);
	dirname = ustr_dup(path);
	ustr_add_cstr(&dirname, "/");

	ustr_add_cstr(gophermap, "1");
	ustr_add(gophermap, name);
	ustr_add_cstr(gophermap, "\t");
	ustr_add_cstr(gophermap, ustr_cstr(path) + 1);
	ustr_add_cstr(gophermap, "/\t");
	ustr_add_cstr(gophermap, hostname);
	ustr_add_cstr(gophermap, "\t70\r\n");

	process_directory(dirname);

	ustr_free(name);
	ustr_free(dirname);

}

void process_dirent(const Ustr *here, const struct dirent *d, Ustr **gophermap)
{
	Ustr *path = USTR("");

	ustr_set_fmt(&path, "%s%s", ustr_cstr(here), d->d_name); /* {here} ends with slash */

	if (dirent_regular_p(d))
		process_dirent_regular(path, gophermap);
	else if (dirent_directory_p(d))
		process_dirent_directory(path, gophermap);

	ustr_free(path);
}

void process_directory(const Ustr *here)
{
	struct dirent **entries;
	int n;
	Ustr *gophermap = USTR("");

	n = scandir(ustr_cstr(here), &entries, &scandir_filter, &alphasort);
	if (n < 0) {
		write2("fatal: failed to scan directory\n");
		printf("%s\n", ustr_cstr(here));
		exit(1);
	}

	while (n--) {
		process_dirent(here, entries[n], &gophermap);
		free(entries[n]);
	}
	free(entries);

	ustr_add_cstr(&gophermap, ".\r\n"); /* Be nice, finalize gophermap */
	cdb_make_add(&cdb, ustr_cstr(here) + 1, ustr_len(here) - 1, ustr_cstr(gophermap), ustr_len(gophermap));
	ustr_free(gophermap);
}

int main (int argc, char **argv)
{
	int fd;
	const char *output;
	Ustr *temp = USTR("");

	if (argc < 2) {
		write2("usage: erinaco-data hostname [output]\n");
		return 1;
	}

	hostname = argv[1];
	output = argc > 2 ? argv[2] : "../data.cdb";

	ustr_set_fmt(&temp, "%s.%d", output, getpid());

	fd = open_readwrite(ustr_cstr(temp));
	if (fd < 0) {
		write2("fatal: failed to open temporary database for writing\n");
		return 1;
	}
	if (cdb_make_start(&cdb, fd) < 0) {
		write2("fatal: failed to initialize database\n");
		return 1;
	}

	process_directory(USTR1(\2, "./")); /* "./" instead of "." is essential */

	if (cdb_make_finish(&cdb) < 0) {
		write2("fatal: failed to finalize database\n");
		return 1;
	}

	if (close(fd) < 0) {
		write2("fatal: failed to close temporary database\n");
		return 1;
	}
	if (rename(ustr_cstr(temp), output) < 0) {
		write2("fatal: failed to rename temporary database\n");
		return 1;
	}
	ustr_free(temp);
	return 0;
}
