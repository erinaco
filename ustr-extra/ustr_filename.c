#include <ustr.h>

Ustr * ustr_filename(const Ustr *path)
{
	Ustr *res = USTR("");
	const char *s, *p;

	s = ustr_cstr(path);
	for (p = s; *p; ++p) {
		if (*p == '/') s = p + 1;
	}

	ustr_set_cstr(&res, s);
	return res;
}

