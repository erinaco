#include <ustr.h>

Ustr * ustr_basename(const Ustr *path)
{
	Ustr *res = USTR("");
	const char *s, *p;

	s = ustr_cstr(path);

	for (p = s; *p; ++p) {
		if (*p == '/') s = p + 1;
	}
	/* Here {s} points to first byte after last slash (or to start of string). */
	for (p = s; *p; ++p) {
		if (*p == '.') break;
	}
	/* Here {p} points to first dot in filename or to end of string. */
	ustr_set_buf(&res, s, p - s);

	return res;
}
