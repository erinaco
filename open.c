#include <unistd.h>
#include <fcntl.h>

int open_read(const char *filename) {
	return open(filename, O_RDONLY);
}

int open_readwrite(const char *filename)
{
	return open(filename, O_RDWR|O_TRUNC|O_CREAT, 0644);
}
