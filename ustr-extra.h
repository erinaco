/* include <ustr.h> */

/* Unlike basename(1) and basename(3), strip all extensions. */
Ustr * ustr_basename(const Ustr *);
Ustr * ustr_filename(const Ustr *);
