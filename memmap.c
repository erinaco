#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "memmap.h"

int mem_map_read (struct memory *mem, int fd)
{
	struct stat sb;
	int err;

	err = fstat(fd, &sb);
	if (err < 0)
		return err;
	/* We assume that file is not modified while we use mapping */
	mem->data = mmap(NULL, sb.st_size, PROT_READ, MAP_SHARED, fd, 0);
	if (mem->data == MAP_FAILED) {
		return -1;
	}
	mem->length = sb.st_size;
	return 0;
}

void mem_unmap (struct memory *mem)
{
	munmap(mem->data, mem->length);
}
