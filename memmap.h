/* include <sys/mman.h> */
struct memory {
	void *data;
	size_t length;
};

int mem_map_read(struct memory *mem, int fd);
void mem_unmap(struct memory *mem);
