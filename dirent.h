/* include <dirent.h> */
/* include <stdbool.h> */

static inline bool dirent_directory_p(const struct dirent *d)
{
	return d->d_type == DT_DIR;
}

static inline bool dirent_regular_p(const struct dirent *d)
{
	return d->d_type == DT_REG;
}
